﻿using System;
using Excel = Microsoft.Office.Interop.Excel;

namespace ParallelWorks_Excel_API
{
    public partial class PW_API_AddIn
    {

        private void PW_API_AddIn_Startup(object sender, System.EventArgs e)
        {
        }

        private void PW_API_AddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(PW_API_AddIn_Startup);
            this.Shutdown += new System.EventHandler(PW_API_AddIn_Shutdown);
        }
        
        #endregion
    }
}
