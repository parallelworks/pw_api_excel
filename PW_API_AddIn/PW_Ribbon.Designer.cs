﻿namespace ParallelWorks_Excel_API
{
    partial class PW_Ribbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public PW_Ribbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.launchers = this.Factory.CreateRibbonGroup();
            this.launch_pw_workflow = this.Factory.CreateRibbonButton();
            this.start_pw = this.Factory.CreateRibbonButton();
            this.upload_pw_files = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.launchers.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.launchers);
            this.tab1.Label = "Parallel.Works";
            this.tab1.Name = "tab1";
            // 
            // launchers
            // 
            this.launchers.Items.Add(this.start_pw);
            this.launchers.Items.Add(this.upload_pw_files);
            this.launchers.Items.Add(this.launch_pw_workflow);
            this.launchers.Name = "launchers";
            // 
            // launchworkflow
            // 
            this.launch_pw_workflow.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.launch_pw_workflow.Image = Properties.Resources.pw_icon;
            this.launch_pw_workflow.Label = "Launch Workflow";
            this.launch_pw_workflow.Name = "launch_pw_workflow";
            this.launch_pw_workflow.ShowImage = true;
            this.launch_pw_workflow.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.LaunchWorkflowEvent);

            // 
            // start parallel works
            // 
            this.start_pw.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.start_pw.Image = Properties.Resources.pw_icon;
            this.start_pw.Label = "Start API";
            this.start_pw.Name = "start_pw";
            this.start_pw.ShowImage = true;
            this.start_pw.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.StartPWEvent);

            // 
            // upload files
            // 
            this.upload_pw_files.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.upload_pw_files.Image = Properties.Resources.pw_icon;
            this.upload_pw_files.Label = "Upload Files";
            this.upload_pw_files.Name = "upload_files";
            this.upload_pw_files.ShowImage = true;
            this.upload_pw_files.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.UploadFilesEvent);


            // 
            // PW_Ribbon
            // 
            this.Name = "PW_Ribbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.PW_Ribbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.launchers.ResumeLayout(false);
            this.launchers.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup launchers;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton launch_pw_workflow;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton start_pw;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton upload_pw_files;
    }

    partial class ThisRibbonCollection
    {
        internal PW_Ribbon PW_Ribbon
        {
            get { return this.GetRibbon<PW_Ribbon>(); }
        }
    }
}
