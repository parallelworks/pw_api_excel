﻿using System;
using Microsoft.Office.Tools.Ribbon;
using Excel = Microsoft.Office.Interop.Excel;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ParallelWorks_Excel_API
{
    public partial class PW_Ribbon
    {

        string pw_url = "https://eval.parallel.works";

        private void PW_Ribbon_Load(object sender, RibbonUIEventArgs e)
        {

        }

        // SET UP THE PARALLEL WORKS SWMM APPLICATION WORKSHEET
        private void StartPWEvent(object sender, RibbonControlEventArgs e)
        {
            Excel.Window window = e.Control.Context;
            Excel.Worksheet aw = ((Excel.Worksheet)window.Application.ActiveSheet);

            // set column widths
            aw.get_Range("A1").EntireColumn.ColumnWidth = 24;
            aw.get_Range("A1").EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;
            aw.get_Range("B1").EntireColumn.ColumnWidth = 18;
            aw.get_Range("B1").EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft;

            // settings values
            aw.get_Range("A1").Font.Bold = true;
            aw.get_Range("A1").Value2 = "Parallel Works Settings";
            aw.get_Range("A2").Value2 = "API Key:";
            aw.get_Range("A3").Value2 = "Workspace Name:";
            aw.get_Range("A4").Value2 = "Workflow Name:";
            aw.get_Range("A5").Value2 = "Target Directory:";
            aw.get_Range("B2").Value2 = "ab95ed1b3c77904725c63ee5d035ae3a";
            aw.get_Range("B3").Value2 = "SWMM Runs";
            aw.get_Range("B4").Value2 = "ch2meval_swmmsweep_api";
            aw.get_Range("B5").Value2 = "c:/epa_swmm";

            // status values
            aw.get_Range("A7").Font.Bold = true;
            aw.get_Range("A7").Value2 = "Status";
            aw.get_Range("A8").Value2 = "Upload:";
            aw.get_Range("A9").Value2 = "Execution:";
            aw.get_Range("B8").Value2 = "waiting to upload";
            aw.get_Range("B9").Value2 = "waiting to execute";

            // default sweep parameter values
            aw.get_Range("A11").Font.Bold = true;
            aw.get_Range("A11").Value2 = "Sweep Parameters";

            string[] pname = { "STARTMONTH", "STARTDAY", "ENDMONTH", "ENDDAY", "STORM", "S1_IMPER", "S2_IMPER", "S3_IMPER", "S4_IMPER", "S5_IMPER", "S6_IMPER", "S7_IMPER" };
            string[] pvals = { "1", "1", "1", "1", "2-yr,10-yr,100-yr", "40:60:10", "0", "0", "0", "0", "0", "0" };

            int startrow = 12;
            for (int i = 0; i < pname.Length; i++)
            {
                aw.get_Range("A"+(i+startrow)).Value2 = pname[i];
                aw.get_Range("B" + (i + startrow)).Value2 = pvals[i];
            }

        }

        // UPLOAD SPECIFIED DIRECTORY TO PARALLEL WORKS
        private void UploadFilesEvent(object sender, RibbonControlEventArgs e)
        {
            Excel.Window window = e.Control.Context;
            Excel.Worksheet aw = ((Excel.Worksheet)window.Application.ActiveSheet);

            // this function will get the user's history id based on the history name
            string api_key = aw.get_Range("B2").Value2;
            string workspace_name = aw.get_Range("B3").Value2;
            string workspace_id = GetHistoryName(pw_url,api_key,workspace_name);

            if (workspace_id != null)
            {

                System.Diagnostics.Debug.WriteLine(workspace_id);

                // write the parameters to a sweep file and place in the targetDirectory
                string targetDirectory = aw.get_Range("B5").Value2;

                if (Directory.Exists(targetDirectory))
                {
                    // remove previous results file if it exists 
                    if (Directory.Exists(targetDirectory + "/results"))
                    {
                        Directory.Delete(targetDirectory + "/results",true);
                    }

                    WriteSweepParameters(targetDirectory, 12, aw);

                    // zip up the targetDirectory to prepare to send to PW
                    string zipPath = ZipTargetDirectory(targetDirectory);

                    // prepare the PW API payload and files
                    NameValueCollection payload = new NameValueCollection();
                    NameValueCollection files = new NameValueCollection();

                    payload.Add("key", api_key);
                    payload.Add("tool_id", "upload1");
                    payload.Add("workspace_id", workspace_id);

                    files.Add("files_0|file_data", zipPath);

                    // send the PW upload request
                    string upload = send_pw_request(pw_url + "/api/tools", payload, files);

                    // poll the upload state until file is complete
                    Excel.Range upload_status = aw.get_Range("B8");
                    string dataset_id = PollForUploadCompletion(pw_url, upload, 1000, upload_status);

                    // set the dataset ID to the EPA SWMM input section
                    upload_status.Value2 = dataset_id;
                }
                else
                {
                    Excel.Range upload_status = aw.get_Range("B8");
                    upload_status.Value2 = "no target directory found";
                }

            }
            else
            {
                Excel.Range upload_status = aw.get_Range("B8");
                upload_status.Value2 = "no PW workspace found";
            }
            
        }

        // EXECUTE EPA SWMM WORKFLOW ON PARALLEL WORKS
        private void LaunchWorkflowEvent(object sender, RibbonControlEventArgs e)
        {
            Excel.Window window = e.Control.Context;
            Excel.Worksheet aw = ((Excel.Worksheet)window.Application.ActiveSheet);

            // information needed for running the workflow
            string api_key = aw.get_Range("B2").Value2;
            string workspace_name = aw.get_Range("B3").Value2;
            string workspace_id = GetHistoryName(pw_url, api_key, workspace_name);
            string workflow_name = aw.get_Range("B4").Value2;
            string targetDirectory = aw.get_Range("B5").Value2;
            string fileID = aw.get_Range("B8").Value2;

            // json outlining the inputs for the workflow
            string inputs = "{\"swmmzip\":{\"values\":[{\"src\":\"hda\",\"id\":\""+fileID+"\"}]}}";

            // assemble the payload
            NameValueCollection payload = new NameValueCollection();
            NameValueCollection files = new NameValueCollection();

            payload.Add("key", api_key);
            payload.Add("tool_id", workflow_name);
            payload.Add("workspace_id", workspace_id);
            payload.Add("inputs",inputs);

            string execute = send_pw_request(pw_url + "/api/tools", payload, files);

            // poll the execution state until job is complete
            Excel.Range execute_status = aw.get_Range("B9");
            string download_url = PollForExecuteCompletion(pw_url, api_key, execute, 1000, execute_status);

            // set the dataset ID to the EPA SWMM
            execute_status.Value2 = download_url;

            // download the result file and unzip to targetDirectory
            WebClient client = new WebClient();
            client.DownloadFile(download_url, targetDirectory+"/results.zip");

            // unzip the result file
            ZipFile.ExtractToDirectory(targetDirectory + "/results.zip", targetDirectory);
            File.Delete(targetDirectory + "/results.zip");

        }

        private static void WriteSweepParameters(string targetDirectory,int startrow, Excel.Worksheet aw)
        {
            // write the sweep parameters to the target directory
            List<string> sweep = new List<string>();
            sweep.Add("pname pvals");
            for (var i = 0; i < 100; i++)
            {
                if (aw.get_Range("A"+(i+startrow)).Value2 != null)
                {
                    sweep.Add(aw.get_Range("A" + (i + startrow)).Value2 + " " + aw.get_Range("B" + (i + startrow)).Value2);
                }
                else
                {
                    break;
                }
            }
            var sweepwrite = String.Join("\n", sweep.ToArray());
            string path = targetDirectory+"/swmm.sweep";
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            File.WriteAllText(path, sweepwrite);
        }

        private static string PollForExecuteCompletion(string pw_url, string api_key, string execute, int waittime, Excel.Range b)
        {
            dynamic response = JsonConvert.DeserializeObject(execute);
            string job_id = response.jobs[0].id;
            System.Diagnostics.Debug.WriteLine("Job ID: "+job_id);
            System.Timers.Timer timer = new System.Timers.Timer(1000);
            string output_id = null;
            while (true)
            {
                timer.Start();
                WebClient client = new WebClient();
                dynamic progress = JsonConvert.DeserializeObject(client.DownloadString(pw_url + "/api/jobs/" + job_id + "?key=" + api_key));
                string state = progress.state;
                System.Diagnostics.Debug.WriteLine("Job Status: " + state);
                b.Value2 = state;
                if (state == "ok")
                {
                    // this is custom for each workflow output name - TODO - MAKE THIS MORE GENERIC
                    output_id = progress.outputs.results.id;
                    break;
                }
            }
            // get the download url
            WebClient client1 = new WebClient();
            dynamic output = JsonConvert.DeserializeObject(client1.DownloadString(pw_url + "/api/datasets/" + output_id + "?key=" + api_key));
            string output_url = pw_url + output.download_url;

            return output_url;
        }

        private static string PollForUploadCompletion(string pw_url,string upload,int waittime,Excel.Range b)
        {
            dynamic response = JsonConvert.DeserializeObject(upload);
            string dataset_id = response.outputs[0].id;
            System.Timers.Timer timer = new System.Timers.Timer(1000);
            while (true)
            {
                timer.Start();
                WebClient client = new WebClient();
                dynamic progress = JsonConvert.DeserializeObject(client.DownloadString(pw_url + "/api/datasets/" + dataset_id));
                string state = progress.state;
                b.Value2 = state;
                if (state == "ok")
                {
                    break;
                }
            }
            return dataset_id;
        }


        private static string ZipTargetDirectory(string targetDirectory)
        {
            string zipPath = targetDirectory + ".zip";
            // delete the zip file if it already exists
            if (File.Exists(zipPath))
            {
                File.Delete(zipPath);
            }
            ZipFile.CreateFromDirectory(targetDirectory, zipPath, CompressionLevel.Fastest, true, new LinuxEncoder());
            return zipPath;
        }

        private static string GetHistoryName(string pw_url, string api_key, string history_name)
        {
            // this function gets the history id from the users history name
            WebClient client = new WebClient();
            dynamic response = JsonConvert.DeserializeObject(client.DownloadString(pw_url + "/api/histories?key=" + api_key));
            string history_id = null;
            foreach (dynamic history in response)
            {
                if (history.name == history_name)
                {
                    history_id = history.id;
                }
            }
            return history_id;
        }

        private static string send_pw_request(string url, NameValueCollection values, NameValueCollection files = null)
        {
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            // The first boundary
            byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
            // The last boundary
            byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            // The first time it itereates, we need to make sure it doesn't put too many new paragraphs down or it completely messes up poor webbrick
            byte[] boundaryBytesF = System.Text.Encoding.ASCII.GetBytes("--" + boundary + "\r\n");

            // Create the request and set parameters
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            request.Method = "POST";
            request.KeepAlive = true;
            request.Credentials = System.Net.CredentialCache.DefaultCredentials;

            // Get request stream
            Stream requestStream = request.GetRequestStream();

            foreach (string key in values.Keys)
            {
                // Write item to stream
                byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}", key, values[key]));
                requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                requestStream.Write(formItemBytes, 0, formItemBytes.Length);
            }

            if (files != null)
            {
                foreach (string key in files.Keys)
                {
                    if (File.Exists(files[key]))
                    {
                        int bytesRead = 0;
                        byte[] buffer = new byte[2048];
                        byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", key, files[key]));
                        requestStream.Write(boundaryBytes, 0, boundaryBytes.Length);
                        requestStream.Write(formItemBytes, 0, formItemBytes.Length);

                        using (FileStream fileStream = new FileStream(files[key], FileMode.Open, FileAccess.Read))
                        {
                            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
                            {
                                // Write file content to stream, byte by byte
                                requestStream.Write(buffer, 0, bytesRead);
                            }

                            fileStream.Close();
                        }
                    }
                }
            }

            // Write trailer and close stream
            requestStream.Write(trailer, 0, trailer.Length);
            requestStream.Close();

            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                return reader.ReadToEnd();
            };
        }

    }
}
