﻿using System.Text;

namespace ParallelWorks_Excel_API
{
    class LinuxEncoder : UTF8Encoding
    {
        public LinuxEncoder()
        {

        }
        public override byte[] GetBytes(string s)
        {
            s = s.Replace("\\", "/");
            return base.GetBytes(s);
        }
    }
}
