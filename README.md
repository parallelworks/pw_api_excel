# Parallel Works Excel API Add-In for EPA SWMM #

This repository is a C# Excel add-in that showcases Parallel Works API endpoints and interaction on a demonstration EPA SWMM parameter sweep workflow. The main Parallel Works API commands are located in the [PW_API_AddIn/PW_Ribbon.cs](https://bitbucket.org/parallelworks/pw_excel_addin/src/0d4662d2058e0914eb691ab08f0e1df98c872d9b/PW_API_AddIn/PW_Ribbon.cs?fileviewer=file-view-default) file.

The add-in places three new buttons into the Excel Ribbon:

1. **Start API** - this button will generate a set of Excel inputs and settings needed by the rest of the workflow. These inputs include defining the Parallel Works API Key (accessible from https://eval.parallel.works/accountowner/settings/#key), native Windows run directory, and parameters for the parametric sweep.
2. **Upload Files** - this button will generate a sweep file from the Excel sweep parameters, zip up a defined target directory, and upload this zip file to parallel works. Polling occurs until the file successfully uploads.
3. **Execute Workflow** - this button uses the file ID of the newly uploaded zip file, executes the swmmsweep_api workflow on Parallel Works, and returns the results to the defined target directory. Please ensure you have the correct Parallel Works workflow installed on your account, as well as the proper workspace name available.

For video demonstrations of the add-in in action, please see the links below:

* [Short Video Overview](https://parallelworks.com/tutorials/api/excel_addin_short.mp4) (only showing Excel environment)
* [Long Video Overview](https://parallelworks.com/tutorials/api/excel_addin_long.mp4) (showing Excel and Parallel Works environments)

*Any questions or concerns, please contact [shaxted@parallelworks.com](mailto:shaxted@parallelworks.com).*